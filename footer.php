<?php $template_uri = get_bloginfo('template_url'); ?>

<!-- footer start -->
<footer>
  <div class="footer-top primary-bg footer-map pos-rel pt-120 pb-80">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="footer-contact-info footer-contact-info-3 mb-40">
            <div class="footer-logo mb-35">
              <a href="#"><img src="<?php echo $template_uri; ?>/img/logo/logo.png" alt=""></a>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="footer-widget mb-40">
            <div class="footer-title">
              <h3>Información</h3>
            </div>
            <div class="footer-menu footer-menu-2">
              <ul>
                <li><a href="/home">Inicio</a></li>
                <li><a href="/servicios">Servicios Clínicos</a></li>
                <li><a href="/nosotros">Nosotros</a></li>
                <li><a href="/contacto">Contacto</a></li>
                <li><a href="/Examenes">Exámenes de diagnóstico</a></li>
                <li><a href="/procedimientos-laser">Procedimientos Láser</a></li>
                <li><a href="/procedimientos-quirurgico ">Procedimientos Quirúrgicos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="footer-widget mb-5">
            <div class="footer-title">
              <h3>Contáctanos</h3>
            </div>
            <div class="footer-emailing">
              <ul>
                <li><i class="far fa-envelope"></i>glaucomalimacenter@gmail.com</li>
                <li><i class="fas fa-phone"></i>(01) 710 0690</li>
                <li><i class="fas fa-phone"></i>(01) 476 1601</li>
                <li><i class="far fa-flag"></i>Av. San Luis 2272, Lima 15037</li>
              </ul>
              <div class="mt-3">
                <ul class="d-flex">
                  <li><a style="color: #647589; font-size: 20px;" target="_blank" href="https://www.facebook.com/glaucomaLimacenter"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a style="color: #647589; font-size: 20px;" target="_blank" href="https://www.youtube.com/channel/UC8bGzSUaUbmmRTd67dEombA"><i class="fab fa-youtube"></i></a></li>
                  <li><a style="color: #647589; font-size: 20px;" target="_blank" href="https://www.instagram.com/clinicaglaucomaLimacenter/"><i class="fab fa-instagram"></i></a></li>
                  <li><a style="color: #647589; font-size: 20px;" target="_blank" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita"><i class="fab fa-whatsapp"></i></a></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom pt-25 pb-20">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <div class="footer-copyright footer-copyright-3 text-center">
            <p>&copy; GlaucomaLimacenter - 2022</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="butons-call">
    <a class="buttons-flotantes btn-youtube" href="https://www.youtube.com/channel/UC8bGzSUaUbmmRTd67dEombA" target="_blank"  type="button">
      <i class="fab fa-youtube"></i>
    </a>
    <!-- <br> -->
    <a  class="buttons-flotantes btn-wsp" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" type="button">
      <i class="fab fa-whatsapp"></i>
    </a>
  </div>
</footer>
<!-- footer end -->

<!-- JS here -->

<script src="<?php echo $template_uri; ?>/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/popper.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/isotope.pkgd.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/one-page-nav-min.js"></script>
<script src="<?php echo $template_uri; ?>/js/slick.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/ajax-form.js"></script>
<script src="<?php echo $template_uri; ?>/js/wow.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/jquery.nice-select.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/jquery.meanmenu.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/jquery.counterup.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/waypoints.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $template_uri; ?>/js/plugins.js"></script>
<script src="<?php echo $template_uri; ?>/js/main.js"></script>
<script>
  $('#scrollUp').click(function() {
    console.log('ssasas');
    window.open('https://www.youtube.com/channel/UC8bGzSUaUbmmRTd67dEombA','_blank');
  });
</script>

</body>

<?php wp_footer(); ?>

</html>