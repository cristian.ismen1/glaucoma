<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>

<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/nosotros/fachada2.png"">
            <div class=" container">
    <div class="row">
      <div class="col-lg-9 col-md-9">
        <div class="page-title">
          <p class="small-text pb-15">Queremos ayudarte</p>
          <h1>Contáctanos</h1>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
        <div class="page-breadcumb">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb ">
              <li class="breadcrumb-item">
                <a href="/home">Home</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Contáctanos</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
    </div>
  </section>
  <!-- hero-area end -->

  <!-- contact-area start -->
  <section class="contact-area pt-120 pb-90" data-background="<?php echo $template_uri; ?>/img/fact/map.png">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4">
          <div class="contact text-center mb-30">
            <i class="fas fa-envelope"></i>
            <h3>Escríbenos</h3>
            <p>glaucomalimacenter@gmail.com</p>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-4">
          <div class="contact text-center mb-30">
            <i class="fas fa-map-marker-alt"></i>
            <h3>Visítanos</h3>
            <p>Av. San Luis 2272, Lima 15037</p>
          </div>
        </div>
        <div class="col-xl-4  col-lg-4 col-md-4 ">
          <div class="contact text-center mb-30">
            <i class="fas fa-phone"></i>
            <h3>Llámanos</h3>
            <p>(01) 710 0690 </p>
            <p>(01) 476 1601</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- contact-area end -->

  <!-- contact-form-area start -->
  <section class="contact-form-area gray-bg pt-100 pb-100">
    <div class="container">
      <div class="form-wrapper">
        <div class="row align-items-center">
          <div class="col-xl-8 col-lg-8">
            <div class="section-title mb-55">
              <p><span></span>AGENDA TU CITA</p>
              <h1>Déjanos tus datos</h1>
            </div>
          </div>
          <div class="col-xl-4 col-lg-3 d-none d-xl-block ">
            <div class="section-link mb-80 text-right">
              <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0"><span>+</span>Reserva tu cita</a>
            </div>
          </div>
        </div>
        <div class="contact-form">
          <?php echo do_shortcode('[contact-form-7 id="1574" title="contacto"]'); ?>
          
          <p class="ajax-response text-center"></p>
        </div>
      </div>
    </div>
  </section>
  <!-- contact-form-area end -->

  <section class="map-area">
  <iframe id="contact-map" class="contact-map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15605.014872068788!2d-76.9957055!3d-12.0947754!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb9b3a62be5f84cac!2sGlaucoma%20Lima%20center!5e0!3m2!1ses-419!2spe!4v1648929397336!5m2!1ses-419!2spe" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    <!-- <div ></div> -->
  </section>
</main>

<?php get_footer(); ?>