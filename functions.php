<?php

add_action('init', 'error_custom_init');
function error_custom_init()
{
  if (!empty($_GET['dev'])) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
  }

  register_nav_menus(array(
    'principal' => __('Menú Principal'),
  ));
}


function inmuebles_type()
{
  $labels = array(
    'name' => 'Servicios',
    'singular_name' => 'Servicio',
    'menu_name' => 'Servicios',
  );

  $args = array(
    'label' => 'Servicios',
    'description' => 'Mi primer servicio',
    'labels' => $labels,
    'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
    'public' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-visibility',
    'can_export' => true,
    'publicly_queryable' => true,
    'rewrite' => true,
    'show_in_rest' => true
  );
  register_post_type('servicios', $args);
};

add_action('init', 'inmuebles_type');

function pgRegisterTax()
{
  $args =
    array(
      'hierarchical' => true,
      'labels' => array(
        'name' => 'Categorías Servicios',
        'singular_name' => 'Categoría Servicios'
      ),
      'show_admin_column' => true,
      'show_in_nav_menu' => true,
      'rewrite' => array('slug' => 'categoria-servicios')
    );

  register_taxonomy('categoria-servicios', array('servicios'), $args);
}

add_action('init', 'pgRegisterTax');
