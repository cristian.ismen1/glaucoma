<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>
<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/servicios/bg/servicios.png">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="page-title">
            <p class="small-text pb-15">Estamos aquí para cuidar sus ojos</p>
            <h1>Nuestros servicios</h1>
          </div>
        </div>
        <div class="col-lg-3 d-flex justify-content-start justify-content-md-end align-items-center">
          <div class="page-breadcumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ">
                <li class="breadcrumb-item">
                  <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Servicios Clinicos</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="servcies-area gray-bg pt-115 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-xl-7 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-75">
            <div class="section-icon">
              <img class="section-back-icon back-icon-left" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <!-- <h5>Departments</h5> -->
              <h1>Servicios Clínicos</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
        <div class="col-xl-5 col-lg-4">
          <div class="section-button text-right d-none d-lg-block pt-80">
            <!-- <a data-animation="fadeInLeft" data-delay=".6s" href="services.html" class="btn btn-icon ml-0"><span>+</span>more services</a> -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/glaucoma.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Glaucoma</a></h3>
                <p>
                  El Glaucoma es una enfermedad que daña al nervio óptico de manera irreversible. Generalmente está relacionada a un incremento de la presión intraocular por el acúmulo de líquido formado en el ojo y su deficiente drenaje.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/catarata.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Catarata</a></h3>
                <p>
                  La catarata es la principal causa de ceguera en el mundo. Se produce porque el lente natural del ojo (cristalino) se opacifica como producto de la edad o como consecuencia de algunas enfermedades. Esto produce alteraciones visuales como visión borrosa, atenuación en los colores, sensibilidad a la luz, visión doble, entre otros.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/pterigion.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Pterigion</a></h3>
                <p>
                  El pterigión es un crecimiento anormal de forma triangular de la conjuntiva del ojo sobre la córnea. Es un problema bastante frecuente en personas que están expuestas mucho tiempo al sol, polvo, químicos, etc.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/deprendimiento.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Desprendimiento de Retina</a></h3>
                <p>
                  Es la separación de la retina a la parte posterior del ojo. Este fenómeno altera la visión llegando incluso a perderla. Se considera un problema grave que necesariamente debe ser evaluado por un oftalmólogo lo antes posible.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/maculares.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Degeneraciones Maculares</a></h3>
                <p>
                  La degeneración macular asociada a la edad es un problema de la parte central de la retina llamada macula. Esto produce una alteración de la visión en él área central del campo visual.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="fact-area fact-map green-bg pos-rel pt-115 pb-60">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-9 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-45">
            <div class="section-text section-text-white pos-rel">
              <h5 class="white-color">Estamos para ayudarte</h5>
              <h1 class="white-color">Sé el primero en ser atendido por nuestros especialistas</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-4">
          <div class="section-button section-button-left mb-30">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon btn-icon-dark ml-0">
              <span>+</span>
              Reserva tu cita  
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>