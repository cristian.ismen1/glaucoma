<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>
<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/servicios/bg/quirurjico.png">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="page-title">
            <p class="small-text pb-15">Estamos aquí para cuidar sus ojos</p>
            <h1>Procedimientos Quirúrgicos</h1>
          </div>
        </div>
        <div class="col-lg-3 d-flex justify-content-start justify-content-md-end align-items-center">
          <div class="page-breadcumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ">
                <li class="breadcrumb-item">
                  <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Procedimientos Quirúrgicos</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="about-area pt-120 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>TRABECULECTOMÍA + MITOMICINA C</h3>
              <p>
							Procedimiento quirúrgico ambulatorio mediante el cual se crea una fístula de drenaje entre la esclera y el iris con la finalidad de disminuir la PIO en pacientes con Glaucoma.
              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>IMPLANTE DISPOSITIVO VÁLVULA AHMED</h3>
              <p>
							Procedimiento quirúrgico ambulatorio mediante el cual se coloca un dispositivo de drenaje Valvulado en la periferia del globo ocular con la finalidad de disminuir la PIO del paciente con Glaucoma avanzado o refractario al tratamiento convencional.


              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>IMPLANTE DISPOSITIVO VÁLVULA AADI</h3>
              <p>
							Procedimiento quirúrgico ambulatorio mediante el cual se coloca un dispositivo de drenaje No Valvulado en la periferia del globo ocular con la finalidad de disminuir la PIO del paciente con Glaucoma avanzado o refractario al tratamiento convencional

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>CIRUGÍA DE CATARATA CON FACOEMULSIFICACIÓN</h3>
              <p>
							Cirugía ambulatoria mediante la cual se extrae la catarata del paciente por medio del ultrasonido realizando incisiones pequeñas en la córnea.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>CIRUGÍA DE CATARATA CON LÁSER FEMTOSEGUNDO
 </h3>
              <p>
							Cirugía ambulatoria mediante la cual se extrae la catarata del paciente por medio del Femto láser, 100 % libre de bisturí

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>CIRUGÍA DE PTERIGION CON PEGAMENTO TISSEEL
</h3>
              <p>
							Cirugía ambulatoria mediante la cual se extrae el pterigión o carnosidad del paciente aplicando una membrana amniótica liofilizada para disminuir el proceso inflamatorio posterior sin necesidad de usar puntos de sutura.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>APLICACIÓN DE MEDICAMENTOS INTRAVÍTREOS</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplican medicamentos dentro de la cámara vítrea del paciente con la finalidad de tratar distintos procesos inflamatorios o infecciosos.
              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>MICROCIRUGÍA DE GLAUCOMA – GATT </h3>
              <p>
							La Trabeculectomía transluminal asistida por gonioscopia se realiza con la finalidad de disminuir la PIO en pacientes con Glaucoma realizando micro aberturas en el tejido de drenaje del ojo (malla trabecular).

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>MICROCIRUGÍA DE GLAUCOMA – KAHOOK
</h3>
              <p>
							Técnica quirúrgica microinvasiva que se realiza con la finalidad de disminuir la PIO de pacientes con Glaucoma extrayendo una porción de la malla trabecular (tejido de drenaje del ojo).

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>VITRECTOMÍA ANTERIOR + IMPLANTE DE LIO AFAQUICO
</h3>
              <p>
							Procedimiento quirúrgico mediante el cual se extrae el Vítreo infiltrado en la cámara anterior del ojo y se coloca un lente intraocular con la finalidad de mejorar la visión.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>VITRECTOMÍA POSTERIOR </h3>
              <p>
							Es una técnica de microcirugía mediante la cual se extrae el vítreo, que es el gel transparente que rellena la cavidad ocular. Este procedimiento puede ser requerido en diversas patologías como Retinopatía diabética, desprendimiento de retina, etc.

              </p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>

  <section class="fact-area fact-map green-bg pos-rel pt-115 pb-60">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-9 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-45">
            <div class="section-text section-text-white pos-rel">
              <h5 class="white-color">Estamos para ayudarte</h5>
              <h1 class="white-color">Sé el primero en ser atendido por nuestros especialistas</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-4">
          <div class="section-button section-button-left mb-30">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon btn-icon-dark ml-0">
              <span>+</span>
              Reserva tu cita
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>