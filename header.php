<?php
wp_head();

$template_uri = get_bloginfo('template_url');
?>

<!doctype html>
<html class="no-js" lang="zxx">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Glaucoma Lima center </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="<?php echo $template_uri; ?>/site.webmanifest">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $template_uri; ?>/img/favicon.png">
  <!-- Place favicon.png in the root directory -->

  <!-- CSS here -->
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/animate.min.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/nice-select.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/all.min.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/meanmenu.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/default.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/style.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/responsive.css">
  <link rel="stylesheet" href="<?php echo $template_uri; ?>/css/custom.css">
   <!-- Pegue este código tan alto en la etiqueta <head> de la página como sea posible: -->
 <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M8273HX');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>

 <!-- Pegue también este código inmediatamente después de la etiqueta <body> de apertura: -->
 <!-- Google Tag Manager (noscript) -->
 <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M8273HX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <!-- header begin -->
  <header>
    <div class="top-bar d-none d-md-block">
      <div class="container">
        <div class="row d-flex align-items-center">
          <div class="col-xl-6 offset-xl-1 col-lg-9 offset-lg-1 col-md-7 offset-md-1">
            <div class="header-info">
              <span><i class="fas fa-phone"></i>(01) 710 0690</span>
              <span><i class="fas fa-phone"></i>(01) 476 1601</span>
              <span><i class="fas fa-envelope"></i> glaucomalimacenter@gmail.com</span>
            </div>
          </div>
          <div class="col-xl-5 col-lg-3 col-md-4">
            <div class="header-top-right-btn f-right">
              <a href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn">Reserva tu cita</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- menu-area -->
    <div class="header-menu-area">
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-lg-3 col-md-5 d-flex align-items-center">
            <div class="logo logo-circle pos-rel">
              <a href="/home"><img src="<?php echo $template_uri; ?>/img/logo/logo.png" alt=""></a>
            </div>
          </div>
          <div>
            
          </div>
          <div class="col-xl-9 col-lg-9 col-md-9">
            <div class="header-right f-right">
              <div class="header-social-icons f-right d-none d-xl-block">
                <ul>
                  <li><a target="_blank" href="https://www.facebook.com/glaucomaLimacenter"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a target="_blank" href="https://www.youtube.com/channel/UC8bGzSUaUbmmRTd67dEombA"><i class="fab fa-youtube"></i></a></li>
                  <li><a target="_blank" href="https://www.instagram.com/clinicaglaucomaLimacenter/"><i class="fab fa-instagram"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="header__menu f-right">
              <nav id="mobile-menu">
                <ul>
                  <li><a href="/home">Inicio</a></li>
                  <li>
                    <a>
                      Servicios
                    </a>
                    <ul class="submenu">
                      <li><a href="/servicios">Servicios Clínicos</a></li>
                      <li><a href="/Examenes">Exámenes de diagnóstico</a></li>
                      <li><a href="/procedimientos-laser">Procedimientos Láser</a></li>
                      <li><a href="/procedimientos-quirurgico">Procedimientos Quirúrgicos</a></li>
                    </ul>
                  </li>
                  <li><a href="/nosotros">Nosotros</a></li>
                  <li><a href="/contacto">Contacto</a></li>
                </ul>
              </nav>
            </div>
          </div>
          <div class="col-12">
            <div class="mobile-menu"></div>
          </div>
        </div>
        <div class="row row-celular-mobile">
          <div class="col-12">
						<h5 class="text-align-center text-center">
							<i class="fas fa-phone"></i>
							<span class="text-center">(01) 710 0690 </span>
						</h5>
						<h5 class="text-align-center text-center">
							<i class="fas fa-phone"></i>
							<span class="text-center">(01) 476 1601 </span>
						</h5>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- header end -->