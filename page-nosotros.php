<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>

<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/nosotros/fachada.png">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9">
          <div class="page-title">
            <p class="small-text pb-15">Estamos aquí para su cuidarte</p>
            <h1>Sobre nosotros</h1>
            <p>
              Somos una clinica especialista en operaciones de Glaucoma y cataratas con los equipos tecnológicos más avanzados en Lima.
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 d-flex justify-content-start justify-content-md-end align-items-center">
          <div class="page-breadcumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ">
                <li class="breadcrumb-item">
                  <a href="/home">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">nosotros</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- hero-area end -->
  <!-- about-area start -->
  <section class="about-area pt-120 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-5">
          <div class="about-left-side pos-rel mb-30">
            <div class="about-front-img pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/nosotros/fachada2.jpg" alt="">
              <!-- <a class="popup-video about-video-btn white-video-btn" href="https://www.youtube.com/watch?v=I3u3lFA9GX4"><i class="fas fa-play"></i></a> -->
            </div>
            <div class="about-shape">
              <img src="<?php echo $template_uri; ?>/img/about/about-shape.png" alt="">
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-lg-7">
          <div class="about-right-side pt-55 mb-30">
            <div class="about-title mb-20">
              <h5>Sobre nosotros</h5>
              <h1>Clínica Glaucoma Lima center</h1>
            </div>
            <!-- <div class="about-text mb-50">
              <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia.</p>
            </div> -->
            <div class="our-destination">
              <div class="single-item mb-30">
                <div class="mv-icon f-left">
                  <img src="<?php echo $template_uri; ?>/img/about/destination-icon-1.png" alt="">
                </div>
                <div class="mv-title fix">
                  <h3>Nuestra misión</h3>
                  <p>
                    Nuestra misión es brindar los mejores servicios oftalmológicos, cumpliendo los más altos estándares de calidad, equipamiento con tecnología de vanguardia y promoviendo la cultura de prevención en salud visual
                  </p>
                </div>
              </div>
              <div class="single-item">
                <div class="mv-icon f-left">
                  <img src="<?php echo $template_uri; ?>/img/about/destination-icon-2.png" alt="">
                </div>
                <div class="mv-title fix">
                  <h3>Nuestra Visión</h3>
                  <p>
                    Ser reconocidos como el mejor centro oftalmológico a nivel nacional para la prevención, diagnóstico y tratamiento de enfermedades visuales.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- about-area end -->


  <section class="team-area pt-115 pb-10">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-7 col-md-10">
          <div class="section-title pos-rel mb-75">
            <div class="section-icon">
              <img class="section-back-icon back-icon-left" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <h5>Nuestro equipo</h5>
              <h1>Team de la salud visual</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-lg-5">
          <div class="section-button text-right d-none d-lg-block pt-80">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0"><span>+</span>Reserva tu cita</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor1.png" alt="">
            </div>
            <div class="team-content">
              <h3>Dr. Walter Sánchez Reyes</h3>
              <h6>Oftalmólogo </h6>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor2.png" alt="">

            </div>
            <div class="team-content">
              <h3>Dr. David Ramos</h3>
              <h6>Médico General</h6>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor3.png" alt="">

            </div>
            <div class="team-content">
              <h3>Diana Zapata</h3>
              <h6>Técnica en enfermería</h6>
            </div>
          </div>
        </div>


      </div>
    </div>
  </section>

  <!-- fact-area end -->
  <!-- testimonials-area start -->
  <!-- <div class="testimonials-area pt-115 pb-120">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
          <div class="section-title text-center pos-rel mb-40">
            <div class="section-icon">
              <img class="section-back-icon" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <h5>Testimonials</h5>
              <h1>Our Clients Says About Us</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="single-testi">
        <div class="row">
          <div class="col-xl-10 offset-xl-1 col-lg-12 col-md-12">
            <div class="testi-box text-center pos-rel">
              <div class="testi-content pos-rel">
                <div class="testi-bg-icon">
                  <img src="<?php echo $template_uri; ?>/img/testimonials/testi-box-bg.png" alt="">
                </div>
                <div class="testi-quato-icon">
                  <img src="<?php echo $template_uri; ?>/img/testimonials/testi-quato-icon.png" alt="">
                </div>
                <div class="text-text-boxx">
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed
                    ut
                    perspiciatis unde omnis iste natus error sit voluptatem. accusantium
                    doloremque laudantium, totam rem aperiam.</p>
                </div>
                <span></span>
              </div>
              <div class="testi-author">
                <h2 class="testi-author-title">Rosalina D. Williamson</h2>
                <span class="testi-author-desination">founder, uithemes</span>
              </div>
              <div class="test-author-icon">
                <img src="<?php echo $template_uri; ?>/img/testimonials/testi-author-icon.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <!-- testimonials-area end -->
  <!-- analysis-area start -->

  <!-- analysis-area end -->
</main>

<?php get_footer(); ?>