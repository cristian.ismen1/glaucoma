<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>
<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/servicios/bg/laser.png">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="page-title">
            <p class="small-text pb-15">Estamos aquí para cuidar sus ojos</p>
            <h1>Procedimientos Láser</h1>
          </div>
        </div>
        <div class="col-lg-3 d-flex justify-content-start justify-content-md-end align-items-center">
          <div class="page-breadcumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ">
                <li class="breadcrumb-item">
                  <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Procedimientos Láser</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="about-area pt-120 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>SLT (Trabeculectoplastía láser selectiva)</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplica la energía del láser en el tejido de drenaje del ojo para disminuir la presión intraocular de paciente con glaucoma primario o secundario de ángulo abierto.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>IRIDOTOMÍA LÁSER</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplica YAG láser al Iris del paciente creando una micro abertura en el tejido. De esta manera prevenimos el desarrollo posterior de un Cierre angular primario agudo o Glaucoma Agudo de ángulo cerrado.
              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>CAPSULOTOMÍA LÁSER
</h3>
              <p>
							Aplicando YAG láser se limpia la opacidad que puede formarse en la cápsula del cristalino luego de una cirugía de catarata.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>IRIDOPLASTIA</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplica láser Argón a la periferia del iris con la finalidad de abrir el sistema de drenaje anterior en pacientes con ángulo cerrado.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>FOTOCOAGULACIÓN DE RETINA
</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplica Láser a la parte periférica y/o posterior de la retina con la finalidad de producir una quemadura terapéutica en un área de la misma.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>PANFOTOCOAGULACIÓN DE RETINA
</h3>
              <p>
							Procedimiento ambulatorio mediante el cual se aplica láser Argón sobre toda la retina para tratar complicaciones de la retinopatía diabética.

              </p>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>CPC LÁSER DIODO (para tratamientos de glaucoma)
</h3>
              <p>
							La ciclofotocoagulación con láser Diodo es un procedimiento ambulatorio mediante el cual se aplica energía en la esclera del paciente cerca del limbo esclerocorneal con la finalidad de disminuir la Presión intraocular disminuyendo la producción de humor acuoso.

              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="fact-area fact-map green-bg pos-rel pt-115 pb-60">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-9 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-45">
            <div class="section-text section-text-white pos-rel">
              <h5 class="white-color">Estamos para ayudarte</h5>
              <h1 class="white-color">Sé el primero en ser atendido por nuestros especialistas</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-4">
          <div class="section-button section-button-left mb-30">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon btn-icon-dark ml-0">
              <span>+</span>
              Reserva tu cita
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>