<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>
<main>
  <!-- hero-area start -->
  <section class="breadcrumb-bg pt-200 pb-180" data-background="<?php echo $template_uri; ?>/img/glaucoma/servicios/bg/examenes.png">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="page-title">
            <p class="small-text pb-15">Estamos aquí para cuidar sus ojos</p>
            <h1>Exámenes</h1>
            <p class="small-text">Son necesarios para poder confirmar los diagnósticos.</p>
          </div>
        </div>
        <div class="col-lg-3 d-flex justify-content-start justify-content-md-end align-items-center">
          <div class="page-breadcumb">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb ">
                <li class="breadcrumb-item">
                  <a href="/">Inicio</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Exámenes</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="about-area pt-120 pb-90">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Tomografía de coherencia óptica - mácula</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Tomografía de coherencia óptica – nervio óptico + células ganglionares</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Tomografía de coherencia óptica – córnea</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Tomografía de coherencia óptica – segmento anterior</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Angiografía sin contraste / Angioplex de nervio óptico</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Angiografía sin contraste / Angioplex de mácula</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>PaquimetrÍa corneal</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Retinografía</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Campo visual </h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Topografía corneal (SIRIUS)</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Ecografía ocular Modo A, Modo B</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Microscopia especular</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Biometría ARGOS (Alcon – última generación)</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Biometría de inmersión SONOMED</h3>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-6">
          <div class="service-box service-box-border text-center mb-30">
            <div class="service-thumb">
              <img src="<?php echo $template_uri; ?>/img/services/service3.png" alt="">
            </div>
            <div class="service-content">
              <h3>Valoración de ojo seco glándula de meibomio (SIRIUS) </h3>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>

  <section class="fact-area fact-map green-bg pos-rel pt-115 pb-60">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-xl-9 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-45">
            <div class="section-text section-text-white pos-rel">
              <h5 class="white-color">Estamos para ayudarte</h5>
              <h1 class="white-color">Sé el primero en ser atendido por nuestros especialistas</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-4">
          <div class="section-button section-button-left mb-30">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon btn-icon-dark ml-0">
              <span>+</span>
              Reserva tu cita
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>