<?php get_header(); ?>
<?php $template_uri = get_bloginfo('template_url'); ?>


<main>
  <section class="hero-area">
    <div class="hero-slider">
      <div class="slider-active">
        <div class="single-slider slider-height d-flex align-items-center slider1" id="slider1" data-background="<?php echo $template_uri; ?>/img/glaucoma/sliders/slider2.jpg">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-8 col-md-10">
                <div class="hero-text">
                  <div class="hero-slider-caption ">
                    <h1 class="text-portada" data-animation="fadeInUp" data-delay=".4s">Nos preocupamos por tu salud visual</h1>
                    <p class="p-portada" data-animation="fadeInUp" data-delay=".6s">Soluciones más avanzadas, de forma personalizada e integral.</p>
                  </div>
                  <div class="hero-slider-btn">
                    <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0"><span>+</span>Reserva tu cita</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="single-slider slider-height d-flex align-items-center slider2" id="slider2" data-background="<?php echo $template_uri; ?>/img/glaucoma/sliders/slider1.jpg">
          <div class="container">
            <div class="row">
              <div class="col-xl-10 col-lg-8 col-md-10">
                <div class="hero-text">
                  <div class="hero-slider-caption ">
                    <h1 class="text-portada" data-animation="fadeInUp" data-delay=".4s">Nos preocupamos por tu salud visual</h1>
                    <p class="p-portada" data-animation="fadeInUp" data-delay=".6s">Soluciones más avanzadas, de forma personalizada e integral.</p>
                  </div>
                  <div class="hero-slider-btn">
                    <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0"><span>+</span>Reserva tu cita</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="doctor-details-area pt-50 pb-10">
    <div class="container">
			<!-- <div class="row">
					<div class="col-xl-7 col-lg-8 col-md-12">
						<div class="section-title pos-rel mb-75">
							<div class="section-text pos-rel">
							<h1 style="font-size: 2rem;">POSTGRADO</h1>
							</div>
							<div class="section-line pos-rel">
								<img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
							</div>
						</div>
					</div>
				</div> -->
      <div class="row">
        <div class="col-xl-7 col-lg-8">
          <article class="doctor-details-box">

            <div class="service-details-feature fix mb-30">
              <div class="ser-fea-box f-left mr-0">
                <div class="ser-fea-icon f-left">
                  <img src="<?php echo $template_uri; ?>/img/services/ser-fea-icon-2.png" alt="">
                </div>
                <div class="ser-fea-list fix pb-30">
                  <h3>Director Médico</h3>
                  <ul>
                    <li><i class="fas fa-check"></i>
											Especialista en Oftalmología. Otorgado por la Universidad Autónoma de Bucaramanga FOSCAL - UNAB (2013 - 2016) (Bucaramanga - Colombia).
                    </li>
                  </ul>
                </div>
              </div>
							<div class="ser-fea-box f-left mr-0">
                <div class="ser-fea-icon f-left">
                  <img src="<?php echo $template_uri; ?>/img/services/ser-fea-icon-2.png" alt="">
                </div>
                <div class="ser-fea-list fix pb-30">
                  <h3>Supra - Especialización</h3>
                  <ul>
                    <li><i class="fas fa-check"></i>
											Fellowship en Glaucoma. Dpto. de Glaucoma FOSCAL (2016 - 2017) (Bucaramanga - Colombia).
                    </li>
										<li><i class="fas fa-check"></i>
											Glaucoma Clinical/Surgical Observership and Research Fellowship. Glaucoma Associates of Texas (2018) (Dallas – Texas – USA)
                    </li>
                  </ul>
                </div>
              </div>
              <div class="ser-fea-box f-left mr-0">
                <div class="ser-fea-icon f-left">
                  <img src="<?php echo $template_uri; ?>/img/services/ser-fea-icon-2.png" alt="">
                </div>
                <div class="ser-fea-list fix">
                  <h3>Entrenamiento quirúrgico </h3>
                  <ul>
                    <li><i class="fas fa-check"></i>Implante de Dispositivos para manejo de Glaucoma.</li>
                    <li><i class="fas fa-check"></i>Implante de MIGS (Minimally Invasive Glaucoma Surgery).</li>
                    <li><i class="fas fa-check"></i>Trabeculectomías.</li>
                    <li><i class="fas fa-check"></i>Manejo de Cataratas.</li>
                    <li><i class="fas fa-check"></i>Manejo de Cataratas complicadas.</li>
                    <li><i class="fas fa-check"></i>Implante de lentes intraoculares monofocales, tóricos, trifocales y multifocales.</li>
                    <li><i class="fas fa-check"></i>Cirugía refractiva (Miopía , Hipermetropía, astigmatismo y presbicia).</li>
                    <li><i class="fas fa-check"></i>Cirujano en implante de lentes ICL ( son lentes intraoculares para pacientes que no sean candidatos para cirugía refractiva con láser)..</li>
                    <li><i class="fas fa-check"></i>Implante de lentes de anclaje iridiano (Artisan) , pre y retro iridiniano (Artisan – ICL).</li>
                    <li><i class="fas fa-check"></i>Manejo de pérdida de vítreo - vitrectomía anterior y vía pars plana.</li>
                  </ul>
                </div>
              </div>
            </div>
          </article>
        </div>
        <div class="col-xl-5 col-lg-4">
          <div class="service-widget mb-20">
            <div class="team-wrapper team-box-2 team-box-3 text-center mb-30">
              <div class="team-thumb">
                <img src="<?php echo $template_uri; ?>/img/glaucoma/home/doctor.png" alt="">
              </div>
              <div class="team-member-info mt-35 mb-35">
                <h3><a href="doctor-details.html">Dr. Walter Sánchez Reyes</a></h3>
                <h6 class="f-500 text-up-case letter-spacing pink-color">Director médico</h6>
              </div>
            </div>
          </div>
          <!-- 
          C.M.P 59494
          R.N.E 32452 -->
        </div>
      </div>
    </div>
  </div>

  <section class="servcies-area gray-bg pt-115 pb-10">
    <div class="container">
      <div class="row">
        <div class="col-xl-7 col-lg-8 col-md-12">
          <div class="section-title pos-rel mb-75">
            <div class="section-icon">
              <img class="section-back-icon back-icon-left" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <h1>Nuestros servicios</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
        <div class="col-xl-5 col-lg-4">
          <div class="section-button text-right d-none d-lg-block pt-80">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/glaucoma.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Glaucoma</a></h3>
                <p>
                  El Glaucoma es una enfermedad que daña al nervio óptico de manera irreversible. Generalmente está relacionada a un incremento de la presión intraocular por el acúmulo de líquido formado en el ojo y su deficiente drenaje.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/catarata.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Catarata</a></h3>
                <p>
                  La catarata es la principal causa de ceguera en el mundo. Se produce porque el lente natural del ojo (cristalino) se opacifica como producto de la edad o como consecuencia de algunas enfermedades. Esto produce alteraciones visuales como visión borrosa, atenuación en los colores, sensibilidad a la luz, visión doble, entre otros.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/pterigion.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Pterigion</a></h3>
                <p>
                  El pterigión es un crecimiento anormal de forma triangular de la conjuntiva del ojo sobre la córnea. Es un problema bastante frecuente en personas que están expuestas mucho tiempo al sol, polvo, químicos, etc.
                </p>
                <div>
                  <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&amp;text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0" tabindex="0" style="animation-delay: 0.6s;"><span>+</span>Reserva tu cita</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/deprendimiento.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Desprendimiento de Retina</a></h3>
                <p>
                  Es la separación de la retina a la parte posterior del ojo. Este fenómeno altera la visión llegando incluso a perderla. Se considera un problema grave que necesariamente debe ser evaluado por un oftalmólogo lo antes posible.

                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="service-box-3 mb-30 text-center">
            <div class="service-thumb">
              <a><img src="<?php echo $template_uri; ?>/img/glaucoma/servicios/maculares.png" alt=""></a>
            </div>
            <div class="service-content-box">
              <div class="service-content">
                <h3><a>Degeneraciones Maculares</a></h3>
                <p>
                  La degeneración macular asociada a la edad es un problema de la parte central de la retina llamada mácula. Esto produce una alteración de la visión en él área central del campo visual.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="team-area pt-115 pb-10">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-7 col-md-10">
          <div class="section-title pos-rel mb-75">
            <div class="section-icon">
              <img class="section-back-icon back-icon-left" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <h5>Nuestro equipo</h5>
              <h1>Team de la salud visual</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
        <div class="col-xl-6 col-lg-5">
          <div class="section-button text-right d-none d-lg-block pt-80">
            <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon ml-0"><span>+</span>Reserva tu cita</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor1.png" alt="">
            </div>
            <div class="team-content">
              <h3>Dr. Walter Sánchez Reyes</h3>
              <h6>Oftalmólogo </h6>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor2.png" alt="">

            </div>
            <div class="team-content">
              <h3>Dr. David Ramos</h3>
              <h6>Médico General</h6>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-6">
          <div class="team-box text-center mb-60">
            <div class="team-thumb mb-45 pos-rel">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/home/team/doctor3.png" alt="">

            </div>
            <div class="team-content">
              <h3>Diana Zapata</h3>
              <h6>Técnica en enfermería</h6>
            </div>
          </div>
        </div>


      </div>
    </div>
  </section>

  <section class="cta-area pos-rel pt-115 pb-120" data-background="<?php echo $template_uri; ?>/img/glaucoma/home/frase.png">
    <div class="container">
      <div class="row">
        <div class="col-xl-10 offset-xl-1 col-md-12">
          <div class="cta-text text-center">
            <div class="section-title pos-rel mb-50">
              <div class="section-text section-text-white pos-rel">
                <h5>Mantenga sus ojos saludables para disfrutar de la vida</h5>
                <h1 class="white-color">Confía en nosotros para ayudarte y hacer que las cosas vuelvan a estar bien</h1>
              </div>
            </div>
            <div class="section-button section-button-left">
              <a data-animation="fadeInLeft" data-delay=".6s" href="https://api.whatsapp.com/send/?phone=51924878833&text=Hola quisiera reservar una cita" target="_blank" class="btn btn-icon btn-icon-green ml-0"><span>+</span>Reserva tu cita</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="testimonials-area pt-115 pb-60">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
          <div class="section-title text-center pos-rel mb-70">
            <div class="section-icon">
              <img class="section-back-icon" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <h5>Testimonios </h5>
              <h1>Pacientes satisfechos</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="custom-row testimonials-activation">
        <div class="col-xl-12">
          <div class="testi-box-2">
            <div class="test-rating-inner d-flex justify-content-between mb-30 align-items-center pr-15">
              <div class="testi-quato-icon testi-quato-icon-green m-0">
                <img src="<?php echo $template_uri; ?>/img/testimonials/testi-quato-icon.png" alt="">
              </div>
              <div class="testi-rating-list">
                <ul>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                </ul>
              </div>
            </div>
            <div class="testi-content-2">
              <h3>Paciente #41</h3>
              <!-- <h3>Lorem ipsum dolor sit amet, consectetur adipil sicing elit, sed do eiusmod tempor.</h3> -->
              <p>La atención es A1 en general! Después de 1 mes de mi operación con el Dr Walter Sánchez Reyes he quedado muy satisfecha con mi vista! Súper recomendado y muy profesional, estoy muy agradecida 😊</p>
            </div>
            <div class="testi-author d-flex align-items-center mt-30">
              <div class="testi-author-icon-2">
                <img src="<?php echo $template_uri; ?>/img/glaucoma/home/clientes/cliente1.png" alt="">
              </div>
              <div class="testi-author-desination-2">
                <h4>Fiorella Rosales</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <div class="testi-box-2">
            <div class="test-rating-inner d-flex justify-content-between mb-30 align-items-center pr-15">
              <div class="testi-quato-icon testi-quato-icon-green m-0">
                <img src="<?php echo $template_uri; ?>/img/testimonials/testi-quato-icon.png" alt="">
              </div>
              <div class="testi-rating-list">
                <ul>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                </ul>
              </div>
            </div>
            <div class="testi-content-2">
              <h3>Paciente #59</h3>

              <!-- <h3>Lorem ipsum dolor sit amet, consectetur adipil sicing elit, sed do eiusmod tempor.</h3> -->
              <p>Buena atención desde la seguridad y cada uno de los trabajadores del establecimiento. El doctor te explica al mínimo detalle de que es lo recomendable y apropiado para el paciente.</p>
            </div>
            <div class="testi-author d-flex align-items-center mt-30">
              <div class="testi-author-icon-2">
                <img src="<?php echo $template_uri; ?>/img/glaucoma/home/clientes/cliente2.png" alt="">
              </div>
              <div class="testi-author-desination-2">
                <h4>Elvia Del Rosario</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <div class="testi-box-2">
            <div class="test-rating-inner d-flex justify-content-between mb-30 align-items-center pr-15">
              <div class="testi-quato-icon testi-quato-icon-green m-0">
                <img src="<?php echo $template_uri; ?>/img/testimonials/testi-quato-icon.png" alt="">
              </div>
              <div class="testi-rating-list">
                <ul>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                  <li><i class="fas fa-star"></i></li>
                </ul>
              </div>
            </div>
            <div class="testi-content-2">
              <h3>Paciente #100</h3>
              <p>Muy buenos especialistas comprometidos con los pacientes. Muy recomendado. 😊👍🏼</p>
            </div>
            <div class="testi-author d-flex align-items-center mt-30">
              <div class="testi-author-icon-2">
                <img src="<?php echo $template_uri; ?>/img/glaucoma/home/clientes/cliente3.png" alt="">
              </div>
              <div class="testi-author-desination-2">
                <h4>Steven Povis Luyo</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="fact gray-bg">
    <div class="container-fluid p-0">
      <div class="row no-gutters d-flex align-items-center">
        <div class="col-xl-5">
          <div class="h6fact-wrapper pt-30">
            <div class="row">
              <div class="col-lg-6 col-md-6">
              <div class="pt-4 pb-4 h4facts-single border-facts text-center mb-30" style="display: flex;justify-content: center;align-items: center;flex-flow: column;">
                  <i class="h4facts-icon h4facts-iconpink"><img src="https://www.glaucomalimacenter.com/wp-content/themes/glaucoma/img/iconos/ojo.png" alt=""></i>
                  <div style="font-size: 2.5rem;display: flex;">
                    <span class="f-600 pink-color">+</span>
                    <span class="counter f-600 pink-color">4000</span>
                  </div>

                  <h5 class="f-500 theme-color">Pacientes atendidos</h5>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-7">
          <div class="h4facts-thumbbox pos-rel text-right">
            <div class="h4facts-thumb" style="width: 100%;">
              <video poster="<?php echo $template_uri; ?>/img/glaucoma/videos/fondo-video.png" style="width: 100%;" src="<?php echo $template_uri; ?>/img/glaucoma/videos/cirugia.mp4" controls ></video>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="testimonials-area pt-115 pb-30">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1 col-md-10 offset-md-1">
          <div class="section-title text-center pos-rel mb-70">
            <div class="section-icon">
              <img class="section-back-icon" src="<?php echo $template_uri; ?>/img/section/section-back-icon.png" alt="">
            </div>
            <div class="section-text pos-rel">
              <!-- <h5>Testimonios </h5> -->
              <h1>SEGUROS Y CONVENIOS</h1>
            </div>
            <div class="section-line pos-rel">
              <img src="<?php echo $template_uri; ?>/img/shape/section-title-line.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div>
        <div class="row">
          <div class="col-sm-12 col-lg-2">
            <div class="d-flex justify-content-center mb-5">
              <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/colegio-seguro.png"" alt="" srcset="">

            </div>
          </div>
          <div class=" col-sm-12 col-lg-2">
              <div class="d-flex justify-content-center mb-5">
                <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/feban.png"" alt="" srcset="">

            </div>
          </div>
          <div class=" col-sm-12 col-lg-2">
                <div class="d-flex justify-content-center mb-5">
                  <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/mapfre.png"" alt="" srcset="">

            </div>
          </div>
          <div class=" col-sm-12 col-lg-2">
                  <div class="d-flex justify-content-center mb-5">
                    <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/pacifico.png"" alt="" srcset="">

            </div>
          </div>
          <div class=" col-sm-12 col-lg-2">
                    <div class="d-flex justify-content-center mb-5">
                      <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/positiva.png"" alt="" srcset="">

            </div>
          </div>
          <div class=" col-sm-12 col-lg-2">
                      <div class="d-flex justify-content-center mb-5">
                        <img src="<?php echo $template_uri; ?>/img/glaucoma/marcas/rimac.png"" alt="" srcset="">

            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

</main>

<?php get_footer(); ?>